#define _DEFAULT_SOURCE

#include "test.h"
#include <unistd.h>
#define SIZE 10000

void* heap;
struct block_header* first;

static bool test_init() {
	printf("Test init starts\n");
	
	heap = heap_init(SIZE);
	
	
	if(heap==NULL) {
		printf("Error in test init\n");
		return false;
	}
	printf("Test init finished successfully\n");
	return true;
}

static bool test_first(){
	printf("Test 1 starts\n");
	size_t size = 700;
	void* block = _malloc(size);
	
	if (block == NULL || first->is_free || first->capacity.bytes!=size) {
		printf("Error in test 1\n");
		return false;
	}
	printf("Showing the heap\n");
	debug_heap(stdout, first);
	
	_free(block);
	
	printf("Test 1 finished successfully\n");
	return true;
}

static bool test_second(){
	printf("Test 2 starts\n");
	const size_t size = 700;
	void* block1 = _malloc(size);
	void* block2 = _malloc(size);
	if (block1==NULL || block2==NULL){
		printf("Error in test 2\n");
		return false;
	}
	printf("Heap before\n");
	debug_heap(stdout, first);
	_free(block1);
	printf("Heap after\n");
	debug_heap(stdout, first);
	struct block_header *data1 = block_get_header(block1);
    	struct block_header *data2 = block_get_header(block2);
	if (data1->is_free == false || data2->is_free == true) {
		printf("Error in test 2\n");
		return false;
	}
	_free(block1);
	printf("Test 2 finished successfully\n");
	return true;
}

static bool test_third(){
	printf("Test 3 starts\n");
	size_t size = 700;
	void* block1 = _malloc(size);
	void* block2 = _malloc(size);
	void* block3 = _malloc(size);
	if (block1==NULL || block2==NULL || block3==NULL){
		printf("Error in test 3\n");
		return false;
	}
	printf("Heap before\n");
	debug_heap(stdout, first);
	
	_free(block1);
	_free(block2);
	printf("Heap after\n");
	debug_heap(stdout, first);
	struct block_header *data1 = block_get_header(block1);
    	struct block_header *data2 = block_get_header(block2);
    	struct block_header *data3 = block_get_header(block3);
	if (data1->is_free == false || data2->is_free == false || data3->is_free == true) {
		printf("Error in test 3\n");
		return false;
	}
	_free(block3);
	printf("Test 3 finished successfully\n");
	return true;
}

static bool test_fourth(){
	printf("Test 4 starts\n");
	size_t size = 9000;
	void* block1 = _malloc(size);
	void* block2 = _malloc(size);
	if (block1==NULL || block2==NULL){
		printf("Error in test 4\n");
		return false;
	}
	printf("Showing the heap\n");
	debug_heap(stdout, first);
	size_t count = 0;
	struct block_header* data = block_get_header(block1);
	while (data->next != NULL) {
        	count = count + 1;
        	data = data->next;
    	}
    	if (count != 2){
    		printf("Error in test 4\n");
		return false;
    	}
    	_free(block1);
    	_free(block2);
    	printf("Test 4 finished successfully\n");
	return true;
}

static bool test_fifth(){
	printf("Test 5 starts\n");
	struct block_header* last = first;
	while(last->next != NULL) last = last->next;
	void* adr = (uint8_t *)last + last->capacity.bytes;
    	if(mmap((uint8_t*)(getpagesize() * ((size_t) adr / getpagesize() + (((size_t) adr % getpagesize()) > 0))), 1000, 			PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0) == 0){
    		printf("Error in test5\n");
		return false;
    	}

	void* block1 = _malloc(3 * SIZE);
	if((uint8_t*) last->next != ((uint8_t*) block1 - offsetof(struct block_header, contents))) {
        	printf("Error in test5\n");
		return false;
   	}
   	printf("Showing the heap\n");
   	debug_heap(stdout, first);
	_free(block1);
    	printf("Test 5 finished successfully\n");
    	return true;
}

bool test(){
	if(!test_init()) return false;
	first = (struct block_header*) heap;
	return test_first() && test_second() && test_third() && test_fourth() && test_fifth();
}

